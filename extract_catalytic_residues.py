#!/usr/bin/env python3

"""Module-script to store catalytic residue info from an M-CSA API .json file into
data structures used by the pipeline and extract the catalytic residues provided 
in the list produced by get_residue_info."""

import sys
import subprocess
from glob import glob

#BioPython imports
from Bio.PDB.MMCIFParser import MMCIFParser
from Bio.PDB.PDBIO import PDBIO, Select
from Bio.SeqUtils import IUPACData
AA_3TO1 = IUPACData.protein_letters_3to1.copy()

#Project imports
from .config import *

class ResidueSelect(Select):
    """To select a subset residues to write coordinates"""
 
    def __init__(self, residue_list):
        super(ResidueSelect, self).__init__()
        self.residue_list = residue_list

    def accept_model(self, model):
        #We only want model 0 for NMR disgraces
        return model.get_id() == 0

    def accept_residue(self, residue):
        return residue in self.residue_list

    def accept_atom(self, atom):
        #We want only altloc A for disordered atoms
        if not atom.is_disordered() or atom.get_altloc() == 'A':
            atom.set_altloc(' ')
            return 1
        else:
            return 0

class CatResExtractor():
    """Extracts the catalytic residues provided in the list produced by get_residue_info.
    The residues are extracted from reference biological assembly mmCIF files saved locally.
    This script is less chaotic now. You have been mildly warned"""

    def __init__(self, entry, resolution_cutoff=None):

        self.resolution_cutoff = resolution_cutoff
        self.function_locations = dict()
        self.entry = int(entry)
        self.mcsa_id = None
        self.pdb_id = None
        self.assembly_no = None
        self.is_reference = None
        self.function_locations = dict()
        self.entry_dir = None
        self.mmcif = None
        self.pdb = None
        self.parser = MMCIFParser(QUIET=True)
        self.pdbio = PDBIO(for_jess=True)
        self.structure = None

    def extract_catalytic_residues(self):
        """Main method to extract the catalytic residues"""
        if self.entry not in CAT_RES_INFO:
            return False
        self.mcsa_id = str(self.entry).zfill(4)
        for pdb, res_info in CAT_RES_INFO[self.entry].items():
            self.pdb_id = pdb[0]
            self.assembly_no = pdb[1]
            self.is_reference = pdb[2]
            try:
                if not self._process_io():
                    continue
                self._build_structure()
                if not self._check_exptl_method:
                    continue
                self._get_residues(res_info)
                self._reformat_for_jess()
                self._save_pdb_output()
            except:
                print('Failed to extract catalytic residues from {}'.format(self.pdb_id))
                return False
        self._split_entry()
        return True
           
    def _process_io(self):
        """Make input/output filenames"""
        try:
            #Create output directory
            if not os.path.isdir(ENTRIES_DIR):
                os.makedirs(ENTRIES_DIR)
            self.entry_dir = '{}/mcsa_{}'.format(ENTRIES_DIR, self.mcsa_id)
            self.mmcif = '{}/{}-assembly-{}.cif'.format(ASSEMBLIES_DIR, self.pdb_id, self.assembly_no)
            #Stop if mmCIF structure file is not there
            if not os.path.isfile(self.mmcif):
                print('     {} does not exist, probably obsolete'.format(self.mmcif))
                return False
            self.pdb = '{}/mcsa_{}.{}.{}.pdb'.format(self.entry_dir, self.mcsa_id, self.pdb_id,\
                    'reference' if self.is_reference else 'cat_residues')
            #Make entry directory
            if not os.path.isdir(self.entry_dir):
                print('     Extracting entry {}'.format(self.mcsa_id))
                os.makedirs(self.entry_dir)
            return True
        except:
            return False

    def _build_structure(self):
        """Use the BioPython parser to build structure"""
        try:
            self.structure = self.parser.get_structure(self.pdb_id, self.mmcif)
            return True
        except:
            return False

    def _check_exptl_method(self):
        """Check experimental method and resolution"""
        if 'nmr' in self.structure.header['structure_method'].lower():
            return False 
        if self.resolution_cutoff:
            if float(self.structure.header['resolution']) > float(self.resolution_cutoff) and \
                    not self.is_reference:
               return False
        return True
            
    def _get_residues(self, cat_res_info):
        """Iterate through cat residues and extract them from the mmCI"""
        self.mmcif_cat_residues = list()
        self.function_locations = dict()
        for res in cat_res_info:
            #Retrieve residue from mmCIF
            try:
                for chain in self.structure[0]:
                    #We want all the duplicate chains in assemblies (for homologues only)
                    if res['chain'] != chain.get_id()[0] and not res['is_reference']:
                        continue
                    else:
                        if res['is_reference']:
                            chain = self.structure[0][res['chain']]
                        #If we have a modified residue
                        if res['resname'] == 'X' or res['function_location'] == 'ptm':
                            for _res in chain:
                                if _res.get_id()[1] == res['auth_resid']:
                                    cat_res = _res
                        else:
                            if res['auth_resid'] in chain:
                                cat_res = chain[res['auth_resid']]
                            else:
                                continue
                        if cat_res not in self.mmcif_cat_residues:
                            self.mmcif_cat_residues.append(cat_res)
            except KeyError:
                if self.is_reference:
                    print(' Could not find residue {} {} {} {} {}'.format(
                        self.pdb_id, res['resname'], res['resid'], res['auth_resid'], res['chain']))
                pass
            except UnboundLocalError:
                pass
            #Store function locations
            res_info = (res['chain'][0], res['auth_resid'])
            #For cases where we have two function locations in a residue
            if res_info in self.function_locations:
                self.function_locations[res_info] = 'main'
            else:
                self.function_locations[res_info] = res['function_location']
            
    def _reformat_for_jess(self):
        """Reformat residues in Jess format"""
        for res in self.mmcif_cat_residues:
            resname = res.get_resname()
            chain = res.get_parent().get_id()[0]
            auth_resid = res.get_id()[1]
            if resname.capitalize() in AA_3TO1:
                funcloc = self.function_locations[(chain, auth_resid)]
            else:
                funcloc = 'ptm'
            atom_names = [name.get_id() for name in res.get_list()]

            if 'main' in funcloc:
                key = 'Main'
            if 'side' in funcloc:
                key = resname.capitalize()
            if 'ptm' in funcloc:
                key = 'Ptm'
            if 'term' in funcloc:
                key = resname.capitalize()

            for name in atom_names:
                #We keep only three endpoint functional atoms
                if name not in RESIDUE_DEFINITIONS[key]:
                    del res[name]
                else:
                #Modify atom id and occupancy in a Jess-style
                    #pfff
                    if res.is_disordered():
                        for datom in res.get_unpacked_list():
                            if datom.get_id() == name:
                                datom.set_serial_number(RESIDUE_DEFINITIONS[key][name][0])
                                datom.set_occupancy(RESIDUE_DEFINITIONS[key][name][1])

                    res[name].set_serial_number(RESIDUE_DEFINITIONS[key][name][0])
                    res[name].set_occupancy(RESIDUE_DEFINITIONS[key][name][1])
        
    def _save_pdb_output(self):
        """Save catalytic residues in PDB format"""
        self.pdbio.set_structure(self.structure)
        self.pdbio.save(self.pdb, ResidueSelect(self.mmcif_cat_residues), preserve_atom_numbering=True)

    def _split_entry(self):
        """Splits the entry directory if it has two references"""

        #Check if entry has two reference structure files. If so, split the directory
        refs = glob('{}/*.reference.pdb'.format(self.entry_dir))
        if len(refs) == 2:
            ref_1 = refs[0].split('/')[-1]
            ref_2 = refs[1].split('/')[-1]

            subprocess.run('cp -r {} {}_1'.format(self.entry_dir, self.entry_dir), shell=True)
            subprocess.run('mv {} {}_2'.format(self.entry_dir, self.entry_dir), shell=True) 
            subprocess.run('rm {}_2/{}'.format(self.entry_dir, ref_1), shell=True)
            subprocess.run('rm {}_1/{}'.format(self.entry_dir, ref_2), shell=True)
            return True
        else:
            return

if __name__ == '__main__':
    print('Starting catalytic residue extraction...')
    extractor = CatResExtractor(sys.argv[1])
    extractor.extract_catalytic_residues()


