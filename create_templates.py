#!/usr/bin/env python3

"""Script-module to extract all individual catalytic sites using as input
the catalytic residues found by extract_catalytic_residues"""

#Python imports
import sys
import os
import re
import subprocess
import shutil
from glob import glob
from natsort import natsorted

#Imports for clustering
from scipy.cluster.hierarchy import linkage, dendrogram
from scipy.spatial.distance import pdist, squareform
from dynamicTreeCut import cutreeHybrid

#Plotting
import matplotlib as mpl
#Do not use X window when drawing plots
mpl.use('Agg')
import matplotlib.pyplot as plt

#Project imports
from .config import *
from .common import pdb_get_residues, pdb_to_atom
from .annotate_site import *
from .superposition import charnley_rms

class TemplateGenerator():

    def __init__(self, entry):
        """Initialise all public entry directory-related variables"""
        self.entry = os.path.abspath(entry).rstrip('/')
        self.entry_name = self.entry.split('/')[-1]
        self.mcsa_id = int(self.entry.split('_')[-1].rstrip('/'))
        self.cat_sites_dir = '{}/{}'.format(SITES_DIR, self.entry_name)
        self.templates_dir = TEMPLATES_DIR
        self.cat_site_wildcard = '{}/{}.*.pdb'.format(self.cat_sites_dir, self.entry_name)
        self.templates_wildcard = '{}/{}.*.pdb'.format(self.templates_dir, self.entry_name)
        self.reference = None

    def generate_catalytic_sites(self, jess_rmsd=jess_rmsd, 
            jess_distance=jess_distance, keep_jess_format=True,
            annotate_sites=True):
        """Takes as input an self.entry directory containing catalytic residues in PDB format
        and extracts all catalytic sites from the homologues by matching the reference 
        residues into them using Jess. It also generates a template by multiple structure
        superposition using ProFit"""

        if not os.path.isdir(TEMPLATES_DIR):
            os.makedirs(TEMPLATES_DIR)

        #See if we have a reference and annotate it
        self.reference = self._get_reference()
        if not self.reference:
            return

        #Check if we have less than 3 residues in the reference
        if self._few_residues(self.reference):
            return

        #Check for homologues
        if self._no_homologues():
            self._set_reference_as_template(annotate=True)
            return

        #Write the lists needed for Jess
        if not self._write_site_lists(self.reference):
            self._set_reference_as_template(annotate=True)
            return

        #Run Jess to identify catalytic sites
        if self.mcsa_id in BOTTLENECKING_ENTRIES:
            jess_rmsd = alt_jess_rmsd
            jess_distance = alt_jess_distance
        if not self._run_jess(jess_rmsd, jess_distance):
            self._set_reference_as_template(annotate=True)
            return

        #Break Jess results into individual pdbs
        self._break_sites()

        #Filter redundant cat sites
        self._filter_redundancy()

        #Reorder atoms in the order of the reference to make
        #RMSD calculations easier
        self._reorder_atoms()

        #Run ProFit to find a template structure
        if not self._find_templates():
            self._set_reference_as_template(annotate=True)
            return

        #Annotate everything
        if annotate_sites:
            for site in glob(self.cat_site_wildcard):
                annotate_site(site, site, keep_jess_format=keep_jess_format)
            for site in glob(self.templates_wildcard):
                annotate_site(site, site, keep_jess_format=keep_jess_format)

        #Delete temporary files
        self._remove_tempfiles(self.cat_sites_dir)

        print('     Entry {}: Catalytic sites generated'.format(self.mcsa_id))

    @staticmethod
    def filter_jess_out(jess_out):
        """Takes the output of Jess and if there are multiple optimal alignments in specific position
        in a given template-query pair, it keeps only the best one (lowest LogE)"""

        logE_values = []
        chains = []
        templates = dict() 
        current_template = []
        previous_target = None
        previous_chains = set() 
        filtered_out = []

        for line in jess_out: 
            if (line.startswith('REMARK')):
                dumpit = False
                previous_resID = None
                previous_atom_chain = None
                current_template.append(line)
                remark_fields = line.split()
                target = remark_fields[1]
                query = remark_fields[3]
                logE = float(remark_fields[-1])
                logE_values.append(logE)

            if (line.startswith('ATOM')):
                atom = pdb_to_atom(line)
                #Add alternative residues
                if '100' not in atom.atomID:
                    if isinstance(RESIDUE_DEFINITIONS[atom.residue.capitalize()], dict):
                        line = line.strip() + " " + RESIDUE_DEFINITIONS[atom.residue.capitalize()][atom.name][1]
                if atom.resID == previous_resID and atom.chain != previous_atom_chain:
                    dumpit = True
                current_template.append(line)
                chains.append(atom.chain.strip())
                previous_resID = atom.resID
                previous_atom_chain = atom.chain

            if (line.startswith('ENDMDL')):
                chains = tuple(chains)
                current_template.append(line)

                if not dumpit:
                    if previous_target == target and previous_chains == chains:
                        if logE == min(logE_values):
                            templates[target, chains] = current_template.copy()
                    if previous_target != target or (previous_target == target and previous_chains != chains):
                        templates[target, chains] = current_template.copy()
                        logE_values.clear()

                current_template.clear()
                previous_target = target
                previous_chains = chains
                chains = []

        for key, template in templates.items():
            for line in template:   
                filtered_out.append(line.strip())
            filtered_out.append('')

        return filtered_out

    #Private functions
    
    def _get_reference(self):
        """Gets reference filename from catalytic site directory"""

        try:
            reference = glob('{}/*.reference.pdb'.format(self.entry))[0] #get reference structure filename
            return reference
        except:
            print('     Entry {}: Skipping, there is no reference (probably NMR structure)'.format(self.mcsa_id))
            return False

    def _few_residues(self, reference):
        """Skip self.entry if reference catalytic site has less than 3 residues"""

        if len(pdb_get_residues(reference)) < 3:
            print('     Entry {}: Skipping, it has less than 3 residues'.format(self.mcsa_id))
            return True
        return False
     
    def _no_homologues(self):
        """Check if homologues are present"""

        if not glob('{}/*.cat_residues.pdb'.format(self.entry)):
            print('     Entry {}: No homologues. Setting reference structure as template'.format(self.mcsa_id))
            return True
        return False

    def _write_site_lists(self, reference):
        """Writes reference and homologues filenames lists to use in Jess"""

        if not os.path.isdir(self.cat_sites_dir):
            os.makedirs(self.cat_sites_dir)
        #List of targets filenames
        with open('{}/targets.temp'.format(self.cat_sites_dir), 'w') as targets_list:
            for f in glob('{}/*.cat_residues.pdb'.format(self.entry)):
                if os.path.getsize(f) >= 80:
                    print(f, file=targets_list)

        #Filename of reference
        with open('{}/reference.temp'.format(self.cat_sites_dir), 'w') as template_list:
            print(reference, file=template_list)

        #If no homologues are found, set reference as template
        if os.path.getsize('{}/targets.temp'.format(self.cat_sites_dir)) == 0:
            self._remove_tempfiles(self.cat_sites_dir)
            print('     Entry {}: No homologues of <2.5Å. Setting reference structure as template'.format(self.mcsa_id))
            return False
        return True

    def _run_jess(self, jess_rmsd, jess_distance):
        """Runs Jess in specified directory and filters output"""

        with open('{}/jessOut.temp'.format(self.cat_sites_dir), 'w+') as jess_out:
            subprocess.run('jess {0}/reference.temp {0}/targets.temp {1} {2} i'.format(self.cat_sites_dir, 
                        jess_rmsd, jess_distance),
                    stdout=jess_out,
                    shell=True)

            #Check if there are results
            if os.path.getsize('{}/jessOut.temp'.format(self.cat_sites_dir)) == 0:
                self._remove_tempfiles(self.cat_sites_dir)
                print('     Entry {}: No Jess match. Setting reference structure as template'.format(self.mcsa_id))
                return False

            #Filter Jess output (keep only alignments with best LogE score) and write matches in PDB file
            jess_out.seek(0)
            with open('{}/{}'.format(self.cat_sites_dir, 'all_sites.temp'), 'w') as all_sites:
                for line in TemplateGenerator.filter_jess_out(jess_out):
                    print(line, file=all_sites)
        return True

    def _break_sites(self):
        """Takes the refined JESS output and writes separate catalytic site PDB files."""

        all_sites_path = '{}/{}'.format(self.cat_sites_dir, 'all_sites.temp')
        if not os.path.isfile(all_sites_path):
            return False
        if not os.path.isdir(self.cat_sites_dir):
            os.makedirs(self.cat_sites_dir)

        site_no = 0
        outfiles = []
        with open(all_sites_path, 'r') as all_sites:
            for line in all_sites:
                if line.startswith('REMARK'):
                    chains = set()
                    site = []
                    site_no += 1

                    #Get MCSA ID and PDB ID from REMARK line
                    pdb_path = line.split()[1] 
                    info = re.search("(\d+).(\d...).cat_residues", line)
                    mcsa_id = info[1]
                    pdb_id = info[2]
                    site.append(line.strip()) 

                elif line.startswith('ATOM'):
                    chain = pdb_to_atom(line).chain
                    chains.add(chain.strip())
                    site.append(line.strip())

                elif line.startswith('ENDMDL'):
                    site.append(line.strip()) 
                    outfile = '{}/{}.{}_{}.cat_site.{}.pdb'.format(self.cat_sites_dir, 
                            self.entry_name, pdb_id, '_'.join(sorted(chains)), 
                            str(site_no).zfill(4))
                    outfiles.append(outfile)
                    with open(outfile, 'w') as f:
                        for i in site:
                            print(i, file=f)
                else:
                    continue

        shutil.copy(self.reference, self.cat_sites_dir)
        return True

    def _filter_redundancy(self):
        """Performs direct all vs all structural comparison of the catalytic
        sites via superposition, and puts the redundant ones in a separate
        directory"""
        
        pdbs = natsorted(glob(self.cat_site_wildcard))
        
        if not os.path.isdir('{}/redundant'.format(self.cat_sites_dir)):
            os.makedirs('{}/redundant'.format(self.cat_sites_dir))
        parser = PDBParser(QUIET=True)
        structures = []
        seen = []

        for pdb in pdbs:
            #Parse structure
            structures.append(parser.get_structure(pdb.split('/')[-1], pdb))

        for p in structures:
            p_name = p.get_id()
            p_pdb_id = p_name.split('.')[1].split('_')[0]

            for q in structures:
                q_name = q.get_id()
                q_pdb_id = q_name.split('.')[1].split('_')[0]
                if p==q or p_pdb_id != q_pdb_id or (q_name,p_name) in seen:
                    continue

                rms = round(charnley_rms(p,q, reorder=True), 3)
                if rms <= 0.1:
                    try:
                        to_move = q_name if 'reference' not in q_name else p_name
                        shutil.move('{}/{}'.format(self.cat_sites_dir, to_move), 
                                '{}/redundant/{}'.format(self.cat_sites_dir, to_move)) 
                    except FileNotFoundError:
                        pass
                seen.append((p_name,q_name))

    def _reorder_atoms(self):
        """Reorders the atoms of the catalytic sites in the order of the reference
        using the Hungarian algorithm"""

        sites = natsorted(glob(self.cat_site_wildcard))
        for site in sites:
            if 'reference' in site:
                continue
            rms, reorder = charnley_rms(self.reference, site, get_index=True) 
            remarks = []
            atoms = []
            rest = []
            with open(site, 'r') as f:
                for line in f:
                    line = line.strip()
                    if line.startswith('ATOM') or line.startswith('HETATM'):
                        atoms.append(line)
                    if line.startswith('REMARK'):
                        remarks.append(line)
                    if line.startswith('END'):
                        rest.append(line)
            reordered_atoms = [atoms[i] for i in reorder]
            tempfile = '{}/reordered_site.temp'.format(self.cat_sites_dir)
            with open(tempfile, 'w') as o:
                print('\n'.join(remarks), file=o)
                print('\n'.join(reordered_atoms), file=o)
                print('\n'.join(rest), file=o)
            os.rename(tempfile, site)
                        
    def _find_templates(self):
        """Makes RMSD matrix of all vs all cat sites and finds templates by
        hierarchical clustering"""

        parser = PDBParser(QUIET=True)
        cat_sites = natsorted(glob(self.cat_site_wildcard))
        shape = (len(cat_sites), len(cat_sites))
        matrix = np.zeros(shape)
        seen = set()

        #Make RMSD matrix
        for i, site_i in enumerate(cat_sites):
            i_name = site_i.split('/')[-1]
            site_i = parser.get_structure('site_i', site_i)
            for j, site_j in enumerate(cat_sites):
                if i == j:
                    continue
                if (j,i) in seen:
                    matrix[i,j] = matrix[j,i]
                    continue
                site_j = parser.get_structure('site_j', site_j)
                seen.add((i,j))
                rms, r = charnley_rms(site_i, site_j, get_index=True)
                matrix[i,j] = round(rms, 3)

        if len(matrix) == 1:
            print('     Entry {}: Not enough structures to derive consensus: Setting reference as template'.\
                    format(self.mcsa_id))
            return False

        #Do hierarchical clustering
        linkage_matrix = linkage(squareform(matrix), "average")

        #Cut tree automatically
        distances = pdist(matrix)
        dendrogram(linkage_matrix)
        plt.savefig('{}/{}.dendrogram.png'.format(self.templates_dir, self.entry_name)) 
        clusters = cutreeHybrid(linkage_matrix, distances, 
                minClusterSize=1, deepSplit=1, verbose=0) 
        cluster_indeces = clusters['labels'].tolist()

        #Make clusters directories and copy structures
        clusters_dir = '{}/clusters'.format(self.cat_sites_dir)
        if not os.path.isdir(clusters_dir):
            os.makedirs(clusters_dir)

        for unique_index in set(sorted(cluster_indeces)): 
            if not os.path.isdir('{}/cluster_{}'.format(clusters_dir, unique_index)):
                os.makedirs('{}/cluster_{}'.format(clusters_dir, unique_index))

            #Copy each site in its cluster and replace residue and atoms names with dummy ones
            model_no = 0
            for i, cluster_index in enumerate(cluster_indeces):
                if cluster_index == unique_index:
                    with open('{}/cluster_{}/{}'.format(clusters_dir, cluster_index, cat_sites[i].split('/')[-1]), 'w') as o:
                        print('MODEL {}'.format(model_no), file=o)
                        model_no += 1
                        with open(cat_sites[i], 'r') as f:
                            dum = 1
                            for line in f:
                                if not line.startswith('ATOM'):
                                    continue
                                line = line[0:13]+'C   DUM X{:>4}'.format(dum, dum)+line[26:-1]
                                dum += 1
                                print(line.strip(), file=o)
                            print('ENDMDL', file=o)

            self._run_profit('{}/cluster_{}'.format(clusters_dir, unique_index), cluster_no=unique_index)

        return True
    
    def _run_profit(self, directory, cluster_no = None):
        """Performs all vs all alignment of the sites in the specified directory, 
        and writes the representative structure as the template as well as a file 
        containing the pairwise RMSD values."""

        #Write list of cat sites for ProFit
        multifile = '{}/multi.temp'.format(directory)
        pdbs = glob('{}/*.pdb'.format(directory))
        with open(multifile, 'w') as f:
            for i in pdbs:
                print(i, file=f)

        #Run ProFit
        p = subprocess.run('printf "MULTI {}/multi.temp\nSETREF\nFIT\nMWRITE\nSETREF" | profit'.format(directory),
                stdout=subprocess.PIPE,
                shell=True)
        profit_out = p.stdout.decode('utf-8').strip()
        try:
            index_line = profit_out.splitlines()[-1]
            index = int(index_line.split()[2])
        except IndexError:
            try:
                index_line = profit_out.splitlines()[-1]
                index = int(index_line.split()[2])
            #When cluster contains only one member
            except ValueError:
                index = 1
            except IndexError:
                index = 1
        except ValueError:
            index = 1

        #Rename fitted pdbs
        fitted_pdbs = glob('{}/*.fit'.format(directory))
        for pdb in fitted_pdbs:
            os.rename(pdb, pdb[:-4]+'.pdb')

        #Set template
        filename = pdbs[index - 1].split('/')[-1]
        fields = filename.split('.')
        template_name = '{}.{}.cluster_{}.template.pdb'.format(fields[0], fields[1], cluster_no)
        shutil.copyfile('{}/{}'.format(self.cat_sites_dir, filename), 
                '{}/{}'.format(self.templates_dir, template_name))
        os.remove(multifile)

        return True

    def _set_reference_as_template(self, annotate=False):
        """Copies the reference into the templates directory, and
        can also annotate it"""

        if annotate:
            annotate_site(self.reference, self.reference, keep_jess_format=keep_jess_format)
        template_filename = '{}/{}.template.pdb'.format(self.templates_dir, self.entry_name)
        shutil.copyfile(self.reference, template_filename)
        self._remove_tempfiles(self.cat_sites_dir)

    def _remove_tempfiles(self, path):
        """Removes .temp files from specified directory""" 
        
        for tempfile in glob('{}/*.temp'.format(path)):
            if os.path.isfile(tempfile):
                os.remove(tempfile)
            else:
                return False
        return True


if __name__ == "__main__":
   
    generator = TemplateGenerator(sys.argv[1])
    generator.generate_catalytic_sites(keep_jess_format=keep_jess_format,
            annotate_sites=annotate_sites)
