#!/usr/bin/env python3

import sys
import os
import subprocess
from config import ASSEMBLIES_DIR

def get_assembly_mmcifs(pdb_assembly):
    """Downloads mmCIF structure files if they are not present in ASSEMBLIES_DIR"""

    try:
        if not os.path.isdir(ASSEMBLIES_DIR):
            os.makedirs(ASSEMBLIES_DIR)
        for pdb_id, assembly_no in pdb_assembly.items():
            cif = '{}-assembly-{}.cif'.format(pdb_id, assembly_no)
            if os.path.isfile('{}/{}'.format(ASSEMBLIES_DIR, cif)):
                continue
            print('Getting PDB entry {}, assembly {}'.format(pdb_id, assembly_no))
            scpPath = "/nfs/services/pdbe/release-data/www-static-content/entry/{}/{}/{}.gz".format(pdb_id[1:3], pdb_id, cif)
            #cmd = "scp ebi:{} {}".format(scpPath, ASSEMBLIES_DIR)
            #cmd = "cp {} {} && gunzip {}/{}.gz".format(scpPath, ASSEMBLIES_DIR, ASSEMBLIES_DIR, cif)
            cmd_1 = "cp {} {}".format(scpPath, ASSEMBLIES_DIR)
            cmd_2 = "gunzip {}/{}.gz".format(ASSEMBLIES_DIR, cif)
            subprocess.call(cmd_1.split(" "))
            subprocess.call(cmd_2.split(" "))
    except:
        print('Error in downloading assembly files', file=sys.stderr)
        return False
    return True
