#!/usr/bin/env python

import os
import json
import csv
from ast import literal_eval
from collections import defaultdict
from .common import transform_chain

"""I/O Constants"""
WORKING_DIR = '/Users/riziotis/ebi/phd'
if not os.path.isdir(WORKING_DIR):
    WORKING_DIR = '/hps/research1/thornton/riziotis/research/phd'
ASSEMBLIES_DIR = '{}/datasets/assembly_cif'.format(WORKING_DIR) #Directory of mmCIF structure files are saved
ENTRIES_DIR = '{}/results/entries'.format(WORKING_DIR) #This is where all entries will be stored
SITES_DIR = '{}/results/cat_sites'.format(WORKING_DIR) #This is where all the identified catalytic sites will be stored
TEMPLATES_DIR = '{}/results/templates'.format(WORKING_DIR) #This is where the templates and superimposed catalytic sites will be stored

"""Catalytic residue functional atoms definitions"""
RESIDUE_DEFINITIONS = { 
    'Main': {'CA': [100, ''], 'N': [100, ''], 'O': [100, '']},
    'Ptm': {'CA': [100, ''], 'C': [100, ''],  'CB': [100, '']},
    'Ala': {'C' : [0, ''], 'CA' : [0, ''], 'CB' : [0, '']},
    'Cys': {'CA' : [0, ''], 'CB' : [0, ''], 'SG' : [0, '']},
    'Asp': {'CG': [3, 'E'], 'OD1': [3, 'E'], 'OD2': [3, 'E']},
    'Glu': {'CD': [3, 'D'], 'OE1': [3, 'D'], 'OE2': [3, 'D']},
    'Phe': {'CE1': [3, ''], 'CE2': [3, ''], 'CZ': [0, '']},
    'Gly': {'N' : [0, ''], 'CA' : [0, ''], 'C' : [0, '']},
    'His': {'CG': [0, ''], 'CD2':[8, ''], 'ND1': [8, '']},
    'Ile': {'CA': [0, 'VL'], 'CB': [0, 'VL'], 'CG1': [3, 'VL']},
    'Lys': {'CD' : [0, ''], 'CE' : [0, ''], 'NZ' : [0, '']},
    'Leu': {'CA': [0, 'VI'], 'CB': [0, 'VI'], 'CG': [3, 'VI']},
    'Met': {'CG' : [0, ''], 'SD' : [0, ''], 'CE' : [0, '']},
    'Asn': {'CG': [3, 'Q'], 'OD1': [1, 'Q'] , 'ND2': [1, 'Q']},
    'Pro': {'C' : [0, ''], 'CA' : [0, ''], 'O' : [0, '']},
    'Gln': {'CD': [3, 'N'], 'OE1': [1, 'N'], 'NE2': [1, 'N']},
    'Arg': {'CZ': [0, ''], 'NH1': [3, ''], 'NH2': [3, '']},
    'Ser': {'CA': [0, 'T'], 'CB':[0, 'T'], 'OG': [3, 'T']},
    'Thr': {'CA': [0, 'S'], 'CB': [0, 'S'], 'OG1': [3, 'S']},
    'Trp': {'NE1' : [0, ''], 'CZ2' : [0, ''], 'CH2' : [0, '']},
    'Tyr': {'CE1': [3, ''], 'CZ': [0, ''], 'OH': [0, '']},
    'Val': {'CA': [0, 'LI'], 'CB': [0, 'LI'], 'CG': [3, 'LI']},
    }

EQUIVALENT_ATOMS = {
        'HIS.CD2' : 'HIS.ND1',
        'ASP.OD2' : 'ASP.OD1',
        'ASN.ND2' : 'ASN.OD1',
        'ARG.NH2' : 'ARG.NH1',
        'PHE.CE2' : 'PHE.CE1',
        'TYR.CE2' : 'TYR.CE1',
        'GLU.CD'  : 'ASP.CG',
        'GLU.OE1' : 'ASP.OD1',
        'GLU.OE2' : 'ASP.OD1',
        'GLN.CD'  : 'ASN.CG',
        'GLN.OE1' : 'ASN.OD1',
        'GLN.NE2' : 'ASN.OD1',
        'SER.CA'  : 'THR.CA',
        'SER.OG'  : 'THR.OG1',
        'SER.CB'  : 'THR.CB',
        'ILE.CA'  : 'LEU.CA',
        'ILE.CB'  : 'LEU.CB',
        'ILE.CG1' : 'LEU.CG',
        'VAL.CA'  : 'LEU.CA',
        'VAL.CB'  : 'LEU.CB',
        'VAL.CG'  : 'LEU.CG',
        }

BOTTLENECKING_ENTRIES = (43, 390, 445, 546)

"""Datasets paths"""
infoJson = '{}/datasets/catalytic_residues_homologues.json'.format(WORKING_DIR)
pdb_ec_csv = '{}/datasets/sifts/pdb_chain_enzyme.csv'.format(WORKING_DIR)
ligandsJson = '{}/datasets/bound_ligands/bound_cognate_similarities/compound_similarities.json'.format(WORKING_DIR)

"""CatResExtractor parameters"""
resolution_cutoff = None #To ignore pdbs with a resolution worse than this cutoff

"""Template_generator parameters"""
jess_rmsd = 6
jess_distance = 9  #Caveat!: It can make it slow if >1.5 times the rmsd cutoff
alt_jess_rmsd = 4  #For bottlenecking entries
alt_jess_distance = 4
keep_jess_format = True #Put this to False to include all residue atoms and
                         #nearby HET components in the sites. If True, they are
                         #formatted as Jess templates
annotate_sites = True #If true, catalytic sites and templates are annotated
                      #with info from PDB, SIFTS. Ligands are also identified
                      #and annotated

def get_residue_info(infoJson):
    """Reads catalytic residue info for all homologues from M-CSA API and stores
    it in a dictionary"""

    #catalytic_residues = defaultdict(list)
    catalytic_residues = defaultdict(lambda:defaultdict(list))
    pdb_assembly = dict()

    with open(infoJson, "r") as jsonfile:
        residues = json.load(jsonfile)
        for res in range(len(residues)):
            for residue in residues[res]["residue_chains"]: 
                mcsa_id = residues[res]["mcsa_id"]
                pdb_id = residue["pdb_id"]              
                resid = residue["resid"]    # resid in sequence
                auth_resid = residue["auth_resid"]    # resid as author likes
                resname = residue["code"].upper()    # residue name in pdb file
                function_location = residues[res]["function_location_abv"]    # e.g. main-N
                if not function_location:
                    function_location = 'side'
                assembly_no = residue["assembly"]    #curated assembly number 
                                                     #(for reference) or biological assembly 
                                                     #number (for homologues)
                if not assembly_no:
                    assembly_no = 1
                is_reference = residue["is_reference"]  
                chain_name = residue["assembly_chain_name"] if is_reference else residue["chain_name"]
                chain_name = transform_chain(chain_name, to_dash=False)

                #Add pdb_id - assembly_no pair to dict for use in getting the mmcif files from PDB
                if pdb_id not in pdb_assembly:
                    pdb_assembly[pdb_id] = assembly_no
                
                catalytic_residues[mcsa_id][(pdb_id, assembly_no, is_reference)].append(
                        {'resid':resid, 
                         'auth_resid':auth_resid, 
                         'resname':resname, 
                         'chain':chain_name, 
                         'function_location':function_location,
                         'is_reference':is_reference})

    return catalytic_residues, pdb_assembly

"""Parse datasets"""
#Catalytic residue info from M-CSA
print('Getting catalytic residue info from M-CSA...')
CAT_RES_INFO, PDB_ASSEMBLY = get_residue_info(infoJson)

#Cognate-bound ligand similarities from PARITY
print('Parsing additional datasets...')
with open(ligandsJson) as infile:
    COMPOUND_SIMILARITIES = {literal_eval(k):float(v) for k, v in json.load(infile).items()}

#PDB ID - E.C. number mapping from SIFTS
with open(pdb_ec_csv) as infile:
    infile.readline()
    reader = csv.reader(infile)
    PDB_EC = {(line[0], line[1]):line[3] for line in reader}

