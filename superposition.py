#!/usr/bin/env python3
"""Structural alignment functions using various programs/algorithms"""

import sys
import os
import warnings
import subprocess
import rmsd
import copy
import numpy as np
from natsort import natsorted
from glob import glob
import pymol2
p = pymol2.PyMOL()
p.start()
from Bio.PDB.PDBParser import PDBParser
from Bio.PDB.Superimposer import Superimposer
from Bio.PDB.Structure import Structure
from Bio import BiopythonWarning
warnings.simplefilter('ignore', BiopythonWarning)
from .common import filter_jess_out
from .config import *

def pymol_rms(str1, str2):
    try:
        p.cmd.load(str1, 'str1')
        p.cmd.load(str2, 'str2')
        #Create alignment object without outlier rejection
        p.cmd.align('str1','str2', cycles=0, object='aln')
        #Superimpose with outlier selection
        p.cmd.align('str1','str2')
        p.cmd.select('s1', selection='str1 and aln')
        p.cmd.select('s2', selection='str2 and aln')
        #Calculate RMSD over all atoms
        #TODO implement fuzzy atom matching for symmetric atoms (HIS, ASN, GLN etc)
        rms = p.cmd.rms_cur('s1','s2', matchmaker=-1)
        p.cmd.delete('all')
        return(rms)
    except pymol.CmdException:
        return

def jess_rms(str1, str2):
    
    #Write list files
    with open("fixed.temp", "w") as f:
        print(str2, file=f)
    with open("moving.temp", "w") as f:
        print(str1, file=f)

    #Run Jess
    with open('jessOut.temp', 'w+') as jessOut:
        subprocess.run('jess moving.temp fixed.temp 6 9',
                stdout=jessOut,
                shell=True)
    with open('jessOut.temp', 'r') as jessOut:
        try:
            rms = float(filter_jess_out(jessOut)[0].split()[2])
        except:
            for tempfile in glob('*.temp'):
                os.remove(tempfile)
            return
        for tempfile in glob('*.temp'):
                os.remove(tempfile)
        return rms

def charnley_rms(str1, str2, reorder=True, allow_symmetrics=True, get_index=False):
    """Calculates RMSD using the Kabsch algorithm from the rmsd module. 
    Can also find the optimal atom alignment using the Hungarian algorithm.
    See https://github.com/charnley/rmsd for more"""

    if not isinstance(str1, Structure) and not isinstance(str2, Structure):
        parser = PDBParser()
        str1 = parser.get_structure("str1", str1)
        str2 = parser.get_structure("str2", str2)

    p = [atom for atom in str1[0].get_atoms()]
    q = [atom for atom in str2[0].get_atoms()]

    #Match any residue for atoms with atom nubmer 100 
    #(for Jess formatted structures)
    if allow_symmetrics:
        for atom in p:
            if atom.serial_number == 100:
                atom.parent.resname = 'ANY'
        for atom in q:
            if atom.serial_number == 100:
                atom.parent.resname = 'ANY'
                
    p_coords = np.stack([atom.get_coord() for atom in p], axis=0)
    q_coords = np.stack([atom.get_coord() for atom in q], axis=0)
    p_atoms = ['{}.{}'.format(atom.parent.resname, atom.name) for atom in p]
    q_atoms = ['{}.{}'.format(atom.parent.resname, atom.name) for atom in q]

    #Match the all equivalent atoms defined in EQUIVALENT_ATOMS
    #in config module
    if allow_symmetrics:
        for i, atom in enumerate(p_atoms):
            if atom in EQUIVALENT_ATOMS:
                p_atoms[i] = EQUIVALENT_ATOMS[atom]
        for i, atom in enumerate(q_atoms):
            if atom in EQUIVALENT_ATOMS:
                q_atoms[i] = EQUIVALENT_ATOMS[atom]

    p_atoms = np.array(p_atoms)
    q_atoms = np.array(q_atoms)

    # Create the centroid of P and Q which is the geometric center of a
    # N-dimensional region and translate P and Q onto that center.
    p_coords -= rmsd.centroid(p_coords)
    q_coords -= rmsd.centroid(q_coords)

    # Reorder atoms using the Hungarian algorithm
    if reorder:
        q_review = rmsd.reorder_hungarian(p_atoms, q_atoms, p_coords, q_coords)
        q_coords = q_coords[q_review]
        q_atoms = q_atoms[q_review]

    #To return the new index as well
    if get_index:
        return rmsd.kabsch_rmsd(p_coords, q_coords), q_review.tolist()
    return rmsd.kabsch_rmsd(p_coords, q_coords)

def bio_rms(str1, str2):
    """Superimposes two pdb structures using Biopython and returns RMSD"""

    parser = PDBParser()
    sup = Superimposer()
    site1 = parser.get_structure("str1", str1)
    site2 = parser.get_structure("str2", str2)

    fixed = [atom for atom in site1[0].get_atoms()]
    moving = [atom for atom in site2[0].get_atoms()]

    sup.set_atoms(fixed, moving)
    return sup.rms

def tm_rms(str1, str2):
    """Superimposes two pdb structures using TM-align and returns RMSD"""

    p = subprocess.run('tmalign {} {} 2>/dev/null | grep RMSD= '.format(str1, str2),
            stdout=subprocess.PIPE,
            shell=True)
    tmalignOut = p.stdout.decode('utf-8').strip()

    try:
        rms = float(tmalignOut.split()[4].strip(','))
        return rms
    except:
        return

def profit_rms(str1, str2):
    """Superimposes two pdb structures using ProFit and returns RMSD"""

    p = subprocess.run('printf "FIT" | profit {} {}'.format(str1, str2),
            stdout=subprocess.PIPE,
            shell=True)
    profitOut = p.stdout.decode('utf-8').strip()
    rmsLine = profitOut.splitlines()[-2]

    try:
        rms = float(rmsLine.split()[1])
        return rms
    except:
        return
