#!/usr/bin/env python

"""Basic classes and functions for the templates pipeline"""

#python imports
import sys
import re
import ssl
import math
import time
import matplotlib.pyplot as plt
from glob import glob
from lxml import etree
from urllib.request import urlopen
from urllib.error import HTTPError
from Bio.PDB.MMCIF2Dict import MMCIF2Dict
from Bio.PDB.MMCIFParser import MMCIFParser

#atoms to be used when creating the Tess/Jess templates. If an atom is a list of two elements, the first
#element is the parameter number as defined in the Tess paper by Wallace et al. 1997 and should
#be put as atom ID (if there is no number, then it is 0). The second element of the list, if present 
#is the one-letter code for the alternative residue to be checked in the target pdb file, and should
#be put in the column after the atomic coordinates
INFO_NAMES = { 
    'Main': {'CA': [100, ''], 'N': [100, ''], 'O': [100, '']},
    'Ptm': {'CA': [100, ''], 'C': [100, ''],  'CB': [100, '']},
    'Ala': {'C', 'CA', 'CB'},
    'Cys': {'CA', 'CB', 'SG'},
    'Asp': {'CG': [0, 'E'], 'OD1': [3, 'E'], 'OD2': [3, 'E']},
    'Glu': {'CD': [0, 'D'], 'OE1': [3, 'D'], 'OE2': [3, 'D']},
    'Phe': {'CE1': [3, ''], 'CE2': [3, ''], 'CZ': [0, '']},
    'Gly': {'N', 'CA', 'C'},
    'His': {'ND1': [-1, ''], 'CE1':[0, ''], 'NE2': [0, '']},
    'Ile': {'CA': [0, 'VL'], 'CB': [0, 'VL'], 'CG1': [3, 'VL']},
    'Lys': {'CD', 'CE', 'NZ'},
    'Leu': {'CA': [0, 'VI'], 'CB': [0, 'VI'], 'CG': [3, 'VI']},
    'Met': {'CG', 'SD', 'CE'},
    'Asn': {'CG': [0, 'Q'], 'OD1': [3, 'Q'] , 'ND2': [3, 'Q']},
    'Pro': {'C', 'CA', 'O'},
    'Gln': {'CD': [0, 'N'], 'OE1': [3, 'N'], 'NE2': [3, 'N']},
    'Arg': {'CZ': [0, ''], 'NH1': [3, ''], 'NH2': [3, '']},
    'Ser': {'CA': [-1, 'T'], 'CB':[-1, 'T'], 'OG': [3, 'T']},
    'Thr': {'CA': [0, 'S'], 'CB': [0, 'S'], 'OG1': [0, 'S']},
    'Trp': {'NE1', 'CZ2', 'CH2'},
    'Tyr': {'CE1': [3, ''], 'CZ': [0, ''], 'OH': [3, '']},
    'Val': {'CA': [0, 'LI'], 'CB': [0, 'LI'], 'CG': [3, 'LI']},
    }

class Atom():
    """Atom class containing, entry ID, atom ID, name, residue, chain, resid and coordinates x,y,z"""

    def __init__(self, pdbID, entryID, atomID, name, residue, chain, resID, x, y, z):
        self.pdbID = pdbID
        self.entryID = entryID
        self.atomID = atomID
        self.name = name.strip()
        self.residue = residue
        self.chain = chain 
        self.resID = resID
        self.x = x
        self.y = y
        self.z = z

    def __repr__(self):
        return __str__(self)

    def __str__(self):
        return "{0.entryID} {0.atomID} {0.residue} {0.name} {0.pdbID}".format(self)
        
    def distance(self, atom_i):
        """Calculates euclidean distance between current atom and atom in argument, using coordinates x,y,z"""
        return math.sqrt((self.x - atom_i.x)**2 + (self.y - atom_i.y)**2 + (self.z - atom_i.z)**2)


class Residue(list):
    """Residue class containing a list of atoms of class Atom()"""

    def __init__(self, pdbID='', resID='', name='', chain=''):
        super()
        self.pdbID = pdbID
        self.resID = resID
        self.name = name
        self.chain = chain

    def __str__(self):
        return '{0.pdbID} {0.resID} {0.name} {0.chain}'.format(self)

    def __eq__(self, residue_i):
        return str(self) == str(residue_i)

    def __ne__(self, residue_i):
        return str(self) != str(residue_i)

    def append(self, atom):
        super().append(atom)
        self.pdbID = self[0].pdbID
        self.resID = self[0].resID
        self.name = self[0].residue
        self.chain = self[0].chain.strip()

        if len(self)>2:
            if self[-1].resID != self[-2].resID:
                print ("Warning: You added an atom of another residue in this residue")

    def distance(self, residue_i):
        """Calculates the minimum distance between this residue and residue in argument""" 

        distances = [] 
        for atom1 in self:
            for atom2 in residue_i:
                distances.append(atom1.distance(atom2))
        return(min(distances))


def pdb_get_residues(pdbPath, pdbID=None, noHet=False):
    """Reads PDB file and returns a list of Residue() objects. If pdbID is not provided, filename is used"""

    if pdbID==None:
        pdbID=pdbPath

    if noHet:
        prefix = "^ATOM"
    else:
        prefix = "^ATOM|HETATM"
    previous_resID = None
    residues = []

    with open(pdbPath, "r") as pdbFile:
        currentRes = Residue()
        for pdbLine in pdbFile:
            if re.search(prefix, pdbLine):
                atom = pdb_to_atom(pdbLine, pdbID)
                if atom.resID != previous_resID or previous_resID == None:  
                    currentRes = Residue()
                    residues.append(currentRes)
                currentRes.append(atom)
                previous_resID = atom.resID
    return residues


def cif2pdb(cif, to_jess_template = False, resolution = '', pdbID = ''):
    """Converts the ATOM and HETATM lines of a input cif list to pdb format. 
    If to_jess_template is True, then the output format is
    a Tess/Jess template, with appropriate atom IDs and alternative residues
    according to Wallace et al. 1997
    
    Caveat!: If you change the alternative residues in the INFO_NAMES, make sure you
    make the same changes to the ALT_RES dictionary in reformat_profitOut() in create_templates_functions"""

    pdb = []
    atoms = []
    identifier = None
    pdb.append('REMARK Catalytic site from {} (Jess format) Resolution: {}'.format(pdbID.upper(), resolution))
    for inline in cif:
        if re.search('^ATOM|HETATM', inline):
            input_line = inline.split()

            prefix = input_line[0]
            altAtom = input_line[4] #if atom occupancy is <1.0, this is the alternative atom identifier
            resName = input_line[5]
            atomName = re.sub("\'|\"", "", input_line[3])
            insCode = input_line[9]
            resChain = transform_chain(input_line[18], to_dash = False) 
            authResid = input_line[16] if not str.isalpha(input_line[16]) else input_line[19]
            coordX = float(input_line[10])
            coordY = float(input_line[11])
            coordZ = float(input_line[12])
            b_factor = float(input_line[14])
            interaction = input_line[-1]
            
            if (to_jess_template):

                #if there are both main chain and side chain interactions for the same residue, keep only the main chain
                if '{} {} {} {}'.format(atomName, resName, resChain, authResid) in atoms:
                    continue

                identifier = '{} {} {} {}'.format(atomName, resName, resChain, authResid)
                atoms.append(identifier)

                if interaction == 'Ptm' or resName.capitalize() not in INFO_NAMES: #If residue is ptm, do residue non-specific search by atom type
                    if (atomName not in INFO_NAMES['Ptm']):
                        continue
                    else:
                        atomID = INFO_NAMES['Ptm'][atomName][0] 
                        occupancy = INFO_NAMES['Ptm'][atomName][1]

                elif interaction == 'Side':
                    if (resName.capitalize() in INFO_NAMES and atomName not in INFO_NAMES[resName.capitalize()]):
                        continue
                    if isinstance(INFO_NAMES[resName.capitalize()], set):
                        atomID = 0
                        occupancy = ''
                    else:
                        atomID = INFO_NAMES[resName.capitalize()][atomName][0]
                        occupancy = INFO_NAMES[resName.capitalize()][atomName][1]
                else:
                    if (atomName not in INFO_NAMES['Main']):
                        continue
                    else:
                        atomID = INFO_NAMES['Main'][atomName][0] 
                        occupancy = INFO_NAMES['Main'][atomName][1]


                pdb_line = ('{:6}{:5d} {:<4}{}{:>3}{:>2}{:>4}{:>12.3f}'
                    '{:>8.3f}{:>8.3f} {:6}'.format( 
                        prefix,
                        atomID,
                        atomName if len(atomName) == 4 else ' {}'.format(atomName),
                        altAtom if altAtom != '.' else ' ',
                        resName.upper(),
                        resChain,
                        authResid,
                        coordX,
                        coordY,
                        coordZ,
                        occupancy))

            else:    
                atomID = int(input_line[1])
                occupancy = float(input_line[13])
                nmrModel = int(input_line[20])

                pdb_line = ('{:6}{:5d} {:<4}{}{:>3}{:>2}{:>4}{:>12.3f}'
                        '{:>8.3f}{:>8.3f}{:>6.2f}{:>6.2f}{:>5}'.format(
                    prefix,
                    atomID,
                    atomName if len(atomName) == 4 else ' {}'.format(atomName),
                    altAtom if altAtom != '.' else ' ',
                    resName.upper(),
                    resChain,
                    authResid,
                    coordX,
                    coordY,
                    coordZ,
                    occupancy,
                    b_factor,
                    nmrModel))

            if altAtom in ['.','A'] and insCode in ['.','?']:
                pdb.append(pdb_line)
    atoms.clear()
    return pdb

def cif_to_atom(cifLine, pdbID): 
    """Reads an ATOM or HETATM line from a cif file, and outputs an Atom() object"""

    if re.search('^ATOM|HETATM', cifLine):
        cifFields = cifLine.split()
        atom = Atom(
                pdbID,
                cifFields[0], #ATOM or HETATM
                cifFields[1], #atomID
                cifFields[3], #atom name
                cifFields[17],#residue
                cifFields[6], #chain
                cifFields[8], #resID (auth_resID)
                float(cifFields[10]),#x
                float(cifFields[11]),#y
                float(cifFields[12]))#z
        return atom
    return False

def pdb_to_atom(pdbLine, pdbID="N/A"):
    """Reads an ATOM or HETATM line from a pdb file and outputs an Atom() object"""
    
    if re.search('^ATOM|HETATM', pdbLine):
        atom = Atom(
                pdbID,          #pdbID
                pdbLine[0:6],   #entryID
                pdbLine[6:11],  #atomID
                pdbLine[13:16], #name
                pdbLine[17:20], #residue
                pdbLine[20:22], #chain
                pdbLine[22:27], #resid
                float(pdbLine[30:38]), #coord x
                float(pdbLine[38:46]), #coord y
                float(pdbLine[46:54])) #coord z
        return atom
    return False

def get_cif_file(pdbID, assembly_no, nmr=False):
    """Gets cif file with biological assembly from PDBe"""
    
    for i in range(5):
        if not assembly_no:
            break
        try:
            if nmr:
                url = "https://www.ebi.ac.uk/pdbe/coordinates/{}/full".format(pdbID)
            else:
                url = "https://www.ebi.ac.uk/pdbe/coordinates/{}/assembly?id={}".format(pdbID, assembly_no)
            gcontext = ssl.SSLContext(ssl.PROTOCOL_TLSv1)
            return urlopen(url, context=gcontext).read().decode('utf-8')
        except HTTPError:
            time.sleep(0.1)
            continue 
    return False

def get_biological_assembly(pdb_id):
    """Returns the preferred assembly number for a pdb"""

    url = ("http://www.ebi.ac.uk/pdbe/static/entry/download/"
           "{}-assembly.xml".format(pdb_id))
    
    for i in range(5):
        try:
            xml_str = urlopen(url).read()
            xml_str = re.sub(b'\sxmlns="[^"]+"', b'', xml_str, count=1)
            assembly_xml_tree = etree.fromstring(xml_str)
            return assembly_xml_tree.find('./assembly[@prefered="True"]').attrib['id']
        except HTTPError:
            time.sleep(0.1)
            continue
    print('Could not find preferred assembly for {}. Probably obsolete entry.'.format(pdb_id))
    return False

def transform_chain(chain, to_dash=False):  # this is not universal, should use assembly identityId instead
    """Transforms the chain field from X-n to XY and reverse"""

    letters = 'ABCDEFGHIJKLMNOPQRSTUVWXYZ'
    if to_dash:
        if len(chain) == 2 and all(c.isalpha for c in chain):
            return "{}-{}".format(chain[0], letters.find(chain[1])+2)
        else:
            return chain
    else:
        if '-' in chain:
            return "{}{}".format(chain.split("-")[0], letters[int(chain.split("-")[1])-2])
        else:
            return chain

def plot_histogram(data, outFile, suptitle='', xlabel='', ylabel='', bins=30, range=[0, 6.0]):
            plt.hist(sorted(data), bins, range)
            plt.suptitle(suptitle, fontsize=18, fontweight='bold')
            plt.xlabel(xlabel, fontsize=16)
            plt.ylabel(ylabel, fontsize=16)
            plt.savefig(outFile)
            plt.close()

def filter_jess_out(jess_out):
    """Takes the output of Jess and if there are multiple optimal alignments in specific position
    in a given template-query pair, it keeps only the best one (lowest LogE)"""

    logE_values = []
    templates = dict()
    chains = []
    current_template = []
    previous_target = None
    previous_chains = set() 
    filtered_out = []

    for line in jess_out: 
        if (line.startswith('REMARK')):
            dumpit = False
            previous_resID = None
            previous_atom_chain = None
            current_template.append(line)
            remark_fields = line.split()
            target = remark_fields[1]
            query = remark_fields[3]
            logE = float(remark_fields[-1])
            logE_values.append(logE)

        if (line.startswith('ATOM')):
            atom = pdb_to_atom(line)
            #Add alternative residues
            if '100' not in atom.atomID:
                if isinstance(RESIDUE_DEFINITIONS[atom.residue.capitalize()], dict):
                    line = line.strip() + " " + RESIDUE_DEFINITIONS[atom.residue.capitalize()][atom.name][1]
            if atom.resID == previous_resID and atom.chain != previous_atom_chain:
                dumpit = True
            current_template.append(line)
            chains.append(atom.chain.strip())
            previous_resID = atom.resID
            previous_atom_chain = atom.chain

        if (line.startswith('ENDMDL')):
            chains = tuple(chains)
            current_template.append(line)

            if not dumpit:
                if previous_target == target and previous_chains == chains:
                    if logE == min(logE_values):
                        templates[target, chains] = current_template.copy()
                if previous_target != target or (previous_target == target and previous_chains != chains):
                    templates[target, chains] = current_template.copy()
                    logE_values.clear()

            current_template.clear()
            previous_target = target
            previous_chains = chains
            chains = []

    for key, template in templates.items():
        for line in template:   
            filtered_out.append(line.strip())
        filtered_out.append('')

    return filtered_out
