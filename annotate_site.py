#!/usr/bin/env python

import sys
import os
import re
from glob import glob
from Bio.PDB.MMCIF2Dict import MMCIF2Dict

from .config import *
from .find_ligands.find_ligands import *
from .common import pdb_to_atom

def annotate_site(sitePath, outFile=None, ligands=True, keep_jess_format=True):
    """Takes a catalytic site and annotates it with REMARK
    lines, with info from the mother assembly mmCIF, EC
    annotation from SIFTS and ligand info""" 

    #Parse filename to get IDs
    filename = sitePath.split('/')[-1]
    info = re.search("mcsa_(\d+_*\d*)\.(\d...)", filename)
    if info:
        mcsa_id = info.group(1)
        pdb_id = info.group(2)
        if 'cat_site' in filename:
            siteNo = re.search("cat_site\.(\d+)\.", filename).group(1)
        else:
            siteNo = 'Template'
    else:
        return

    #Get parent mmCIF filename
    mmcifPath = glob('{}/{}*'.format(ASSEMBLIES_DIR, pdb_id))[0]

    #Find ligands in catalytic site
    if ligands or keep_jess_format == False:
        ligand_remarks, ligand_coords = find_ligands(
                sitePath, mmcifPath, headroom=1, 
                write_coords=True, all_coords=True)
        if not ligand_coords:
            return

    #Get coordinates
    coords = []
    with open(sitePath, 'r') as site:
        chains = set()
        for line in site:
            if line.startswith('ATOM') or line.startswith('HETATM'):
                atom = pdb_to_atom(line)
                chain = atom.chain
                chains.add(chain.strip())
                coords.append(line.strip()) 
            if line.startswith('ENDMDL'):
                coords.append(line.strip()) 

    #Get EC number from SIFTS dataset
    if (pdb_id, list(chains)[0][0]) in PDB_EC:
        ec = PDB_EC[(pdb_id, list(chains)[0][0])]
    else:
        ec = 'N/A'
    #Get info from mmCIF mother files
    remarks = []
    mmcif = MMCIF2Dict(mmcifPath) 

    assemblyId = mmcif['_entity_poly.assembly_id']
    title = mmcif['_struct.title']
    enzyme = mmcif['_struct.pdbx_descriptor']
    exptlMethod = mmcif['_exptl.method']
    if isinstance(exptlMethod, list):
        exptlMethod = exptlMethod[0]
    if 'nmr' in exptlMethod.lower():
        resolution = 'N/A'
    elif 'microscopy' in exptlMethod.lower():
        resolution = mmcif['_em_3d_reconstruction.resolution']
    else:
        try:
            resolution = mmcif['_refine.ls_d_res_high']
        except:
            resolution = '?'
    try:
        organismName = mmcif['_entity_src_nat.pdbx_organism_scientific']
    except KeyError:
        try:
            organismName = mmcif['_entity_src_gen.pdbx_gene_src_scientific_name']
        except KeyError:
            organismName = '?'
    try:
        organismId = mmcif['_entity_src_nat.pdbx_ncbi_taxonomy_id']
    except KeyError:
        try:
            organismId = mmcif['_entity_src_gen.pdbx_gene_ncbi_taxonomy_id']
        except KeyError:
            organismId = '?'
    try:
        hostName =  mmcif['_entity_src_gen.pdbx_host_org_scientific_name']
    except KeyError:
        hostName = '?'
    try:
        hostId = mmcif['_entity_src_gen.pdbx_host_org_ncbi_taxonomy_id']
    except:
        hostId = '?'

    if ('cat_site' in filename):
        remarks.append('REMARK CATALYTIC SITE')
        remarks.append('REMARK M-CSA ENTRY {} SITE {}'.format(int(mcsa_id), int(siteNo))) 
    else:
        remarks.append('REMARK CATALYTIC RESIDUES')
        remarks.append('REMARK M-CSA ENTRY {}'.format(int(mcsa_id))) 
    remarks.append('REMARK PDB_ID {}'.format(pdb_id.upper()))
    remarks.append('REMARK ASSEMBLY_ID \'{}\''.format(assemblyId)) 
    remarks.append('REMARK CHAINS {}'.format(', '.join(sorted(chains))))
    remarks.append('REMARK TITLE \'{}\''.format(title)) 
    remarks.append('REMARK ENZYME \'{}\''.format(enzyme)) 
    remarks.append('REMARK EC \'{}\''.format(ec))
    remarks.append('REMARK EXP_METHOD \'{}\''.format(exptlMethod)) 
    remarks.append('REMARK RESOLUTION {}'.format(resolution)) 
    remarks.append('REMARK ORGANISM \'{}\' {}'.format(organismName, organismId)) 
    remarks.append('REMARK HOST \'{}\' {}'.format(hostName, hostId)) 
    if ligands and ligand_remarks:
        remarks.append(ligand_remarks)
    
    #Write everything
    if not outFile:
        if 'reference' in sitePath:
            newPdbid = '{}_{}'.format(pdb_id, '_'.join(sorted(chains)))
            sitePath = re.sub("\.\d...\.reference", ".{}.reference".format(newPdbid), sitePath)
        noSuffix = re.search('(.+)\.pdb', sitePath).group(1)
        outFile = ('{}.pdb'.format(noSuffix))
    with open(outFile, 'w') as f:
        for i in remarks:
            print(i, file=f)
        if keep_jess_format:
            for i in coords:
                print(i, file=f)
        else:
            for i in ligand_coords:
                print(i, file=f)
    return True

