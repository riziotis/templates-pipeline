#!/usr/bin/env python
"""Reads a catalytic residues PDB and its parent structure file,
(PDB or MMCIF), and searches for ligands close to the catalytic 
residues.It can also work the other way round, by taking as input a
HET code, to search for instances of it in the specified structure
and output the pocket residues coordinates around the ligand within
an arbitrary distance. It works by defining a box around the query
residues with boundaries defined by the coordinates of the residues
in the parent structure, plus a safe zone (headroom argument) around 
(default 5Å).
"""
import sys
import os
import json
import numpy as np
from io import StringIO
from Bio.PDB.PDBParser import PDBParser
from Bio.PDB.MMCIFParser import MMCIFParser
from Bio.PDB.PDBIO import PDBIO, Select
from Bio.PDB.Superimposer import Superimposer
from ..config import COMPOUND_SIMILARITIES

def find_ligands(query, parent_structure, headroom=5, write_coords=False, all_coords=False):
    """Main function. It reads the parent structure and depending on what query user has
    provided (HET code or a subset of residues in PDB format (e.g. a structural template)),
    it searches for the query in the parent file. The arguments are the following:
        -query : HET code string (e.g. COA) or subset of residues in PDB format
        -parent_structure : The structure to search in (PDB or MMCIF format)
        -headroom : the distance around the query to include hits
        -write_coords : 'True' if we want to write PDB coordinates of the results.
                        'False' to output only the residue three-letter codes
        -all_coords : *only when searching with a subset of residues* To output
                      coordinates of all residues (query and hits), transformed in
                      the coordinate frame of the query.
    """

    #Initialize mmcif_parsers
    mmcif_parser = MMCIFParser(QUIET=True)
    pdb_parser = PDBParser(QUIET=True)

    #Define data structures
    cat_residues = []
    all_hets_in_structure = []
    all_hets_in_box = []
    ligand_residues= []
    results = []

    #Parse query input
    try:
        cat_site = pdb_parser.get_structure('cat_site', query)
        by_hetcode = False
    except:
        by_hetcode = True
        query_hetcode = query

    #Parse parent structure
    pdb_id = parent_structure.split('/')[-1][:4] \
            if len(parent_structure.split('/')) > 3 else 'parent_str'
    try:
        structure = mmcif_parser.get_structure(pdb_id, parent_structure)
    except:
        try:
            structure = pdb_parser.get_structure(pdb_id, parent_structure)
        except:
            print('Failed to parse structure {}'.format(pdb_id))
            return False, False

    if by_hetcode:
        #Construct a box around each HET
        boxes = []
        structure[0].get_residues()
        for residue in structure[0].get_residues():
            residue_id = residue.get_id()
            hetfield = residue_id[0]
            if hetfield == 'H_{}'.format(query_hetcode): #To get only the specified HET residues.
                residue_list = [residue]
                boxes.append(Box(residue_list, headroom))
        #Get residues around ligand
        for residue in structure[0].get_residues():
            for box in boxes:
                if residue in box:
                    results.append(residue)
        if len(results) == 0:
            return False, False
        results_remarks = 'REMARK RESIDUES {}'.format(
                                  list(residue.get_resname() for residue in results))

    else:
        #Get coordinates of catalytic residues from mmCIF
        residue_list = []
        for residue in cat_site.get_residues():
            chain = residue.get_parent().get_id()
            resid = residue.get_id()[1]
            try:
                residue_list.append(structure[0][chain][resid])
            except KeyError:
                try:
                    for res in structure[0][chain]:
                        if res.get_id()[1] == resid:
                            residue_list.append(res)
                except:
                    print('Warning while searching for ligands:\n' 
                          'Could not find residue {} {} from {}'.format(chain, resid, pdb_id))
                    continue
        if len(residue_list) == 0:
            return False, False


        #Define box
        box = Box(residue_list, headroom)

        #Search for ligands in the box
        meandists = []
        similarities = []
        for residue in structure[0].get_residues():
            residue_id = residue.get_id()
            hetfield = residue_id[0]
            #Here we capture all the HETs in the structure
            if hetfield[0] == 'H': 
                all_hets_in_structure.append(residue)
                #Here we capture the HETs residing in the box
                if residue in box:
                    all_hets_in_box.append(residue)
                    #Here we check if the HET looks like a cognate and is close
                    #enough to the catalytic residues
                    similarity = Box.similarity_with_cognate(residue)
                    meandist = box.mean_distance_from_residues(residue)
                    similarities.append(similarity)
                    meandists.append(meandist)
                    if similarity and meandist:
                        if similarity >= 0.4 and meandist <= 6:
                            #We are now pretty sure we have a cognate-like ligand
                            ligand_residues.append(residue)
    
        if all_coords:
            results = all_hets_in_box + residue_list
        else:
            results = all_hets_in_box

        #Transform coordinates to the query coordinate frame
        rotran = Box.get_transformation_matrix(structure, cat_site)
        Box.transform_coords(results, rotran)

        #Format the results HET codes as REMARK lines
        results_remarks = 'REMARK ALL_HETS {}\n' \
                          'REMARK NEARBY_HETS {}\n' \
                          'REMARK LIGANDS_IN_SITE {}\n' \
                          'REMARK HET_COGNATE_SIMILARITY {}\n' \
                          'REMARK MEAN_HET_CATRES_DISTANCE {}'.format(
                                  list((residue.get_resname(), residue.get_id()[1]) for residue in all_hets_in_structure),
                                  list((residue.get_resname(), residue.get_id()[1]) for residue in all_hets_in_box),
                                  list((residue.get_resname(), residue.get_id()[1]) for residue in ligand_residues), 
                                  list(i for i in similarities),
                                  list(i for i in meandists))

    #If we want to write coordinates
    if write_coords:
        try:
            io = PDBIO()
            io.set_structure(structure)
            results_pdb = StringIO()
            io.save(results_pdb, ResidueSelect(results))
            results_pdb = results_pdb.getvalue().splitlines()
            return results_remarks, results_pdb            
        except TypeError:
            print('{} may contain two-character chain IDs. Please e-mail\n \
                   riziotis@ebi.ac.uk for a fix'.format(parent_structure))
            return False, False
    else:
        return results_remarks
    
class Box:

    def __init__(self, residue_list, headroom=1):
        """Define a box using the provided residues coordinates adding
        some safe distance around (headroom)"""

        self.residue_list = residue_list
        self.headroom = float(headroom)
        self.points = self._get_boundaries(self.residue_list, self.headroom)
        if not self.points:
            return 
    
    def __str__(self):
        """Print box vertices in PDB format (useful for pocket visualisation"""

        out = []
        string_format = 'ATOM      1  DUM DUM X   0   {0[0]:8.3f}{0[1]:8.3f}{0[2]:8.3f}'

        points = ((self.points['min_x'], self.points['min_y'], self.points['min_z']),
                  (self.points['max_x'], self.points['min_y'], self.points['min_z']),
                  (self.points['min_x'], self.points['max_y'], self.points['min_z']),
                  (self.points['max_x'], self.points['max_y'], self.points['min_z']),
                  (self.points['min_x'], self.points['min_y'], self.points['max_z']),
                  (self.points['min_x'], self.points['max_y'], self.points['max_z']),
                  (self.points['max_x'], self.points['min_y'], self.points['max_z']),
                  (self.points['max_x'], self.points['max_y'], self.points['max_z']))

        for coords in points:
            out.append(string_format.format(coords))        

        return str('\n'.join(out))

    def __contains__(self, residue):
        """Checks if the provided residue is in the box"""

        for atom in residue:
            x = atom.get_coord()[0]
            y = atom.get_coord()[1]
            z = atom.get_coord()[2]

            if self.points['min_x'] <= x <= self.points['max_x'] and \
               self.points['min_y'] <= y <= self.points['max_y'] and \
               self.points['min_z'] <= z <= self.points['max_z']:

               return True
        return False

    def _get_boundaries(self, residue_list, headroom):
        """Parse residues (BioPython objects) and get the coordinates
        to make the box"""
        
        self.coords_x = []
        self.coords_y = []
        self.coords_z = []

        try:
            for residue in self.residue_list:
                for atom in residue:
                    self.coords_x.append(atom.get_coord()[0])
                    self.coords_y.append(atom.get_coord()[1])
                    self.coords_z.append(atom.get_coord()[2])
        except KeyError:
            print('Warning: Error occured while trying to parse structure')
            return

        min_x = min(self.coords_x) - headroom
        max_x = max(self.coords_x) + headroom
        min_y = min(self.coords_y) - headroom
        max_y = max(self.coords_y) + headroom
        min_z = min(self.coords_z) - headroom
        max_z = max(self.coords_z) + headroom
    
        return {'min_x':min_x, 'max_x':max_x,
                'min_y':min_y, 'max_y':max_y,
                'min_z':min_z, 'max_z':max_z}

    def mean_distance_from_residues(self, component):
        """Calculates the mean distance of an arbitrary residue (protein or HET) 
        from the residues that define the box"""
        
        dist_sum = 0
        nofres = len(self.residue_list)

        for residue in self.residue_list:
            dist_sum += Box.min_distance(residue, component)

        return round(dist_sum/nofres, 3)

    @staticmethod
    def similarity_with_cognate(component):
        """Checks the similarity score of the given compound with the cognate 
        ligand of the given pdb, using the PARITY-derived data
        (COMPOUND_SIMILARITIES.json) from Jon"""

        try:
            pdb_id = component.get_full_id()[0]
            hetcode = component.get_resname()
        except:
            return None

        r_key = (pdb_id, hetcode, 'r')
        p_key = (pdb_id, hetcode, 'p')
        if r_key in COMPOUND_SIMILARITIES:
            return float(COMPOUND_SIMILARITIES[r_key])
            #if COMPOUND_SIMILARITIES[r_key] >= cutoff:
            #    return True
        elif p_key in COMPOUND_SIMILARITIES:
            return float(COMPOUND_SIMILARITIES[p_key])
            #if COMPOUND_SIMILARITIES[p_key] >= cutoff:
            #    return True
        else:
            #return False
            return None

    @staticmethod
    def get_transformation_matrix(raw_structure, transformed_structure):
        sup = Superimposer()
        raw_atoms = []
        transformed_atoms = []
        raw_coords = []
        transformed_coords = []

        for atom in transformed_structure.get_atoms():
            transformed_atoms.append(atom)
        for atom in transformed_atoms:
            chain = atom.get_parent().get_parent().get_id()
            resid = atom.get_parent().get_id()[1]
            name = atom.get_id()
            try:
                raw_atoms.append(raw_structure[0][chain][resid][name])
            except KeyError: #Damn ptms
                for res in raw_structure[0][chain]:
                    if res.get_id()[1] == resid:
                        raw_atoms.append(res[name])

        for atom in raw_atoms:
            raw_coords.append(atom.get_coord())
        for atom in transformed_atoms:
            transformed_coords.append(atom.get_coord())

        sup.set_atoms(transformed_atoms, raw_atoms)
        return(sup)
        
    @staticmethod
    def transform_coords(residues, sup):

        sup.apply(residues)
        return True
        
    @staticmethod
    def min_distance(residue_i, residue_j):
        """Calculates the minimum distance between this residue and residue in argument""" 

        distances = [] 
        for atom_i in residue_i :
            for atom_j in residue_j:
                distances.append(atom_i-atom_j)
        return(min(distances))

class ResidueSelect(Select):                                                                                                                                             
    """To select the ligand residues to write coordinates"""
 
    def __init__(self, residues):
        super(ResidueSelect, self).__init__()
        self.residues = residues
 
    def accept_residue(self, residue):
        if residue in self.residues:
        #if residue.get_full_id() in self.residues:
            return 1                                                                                                                                                     
        else:                                                                                                                                                            
            return 0  


if __name__ == "__main__":

    #Parse command line arguments
    if len(sys.argv)>=4:
        query = sys.argv[1]
        structure = sys.argv[2]
        headroom = float(sys.argv[3])
        try:
            if sys.argv[4] == '--all-coords':
                all_coords = True
        except:
            all_coords = False

    else:
        print('Usage: find_ligands.py <residues PDB or HET code> <structure> <safe zone distance> [--all-coords]\n \
                    Options: --all-coords: Only when searching with a residue subset PDB.\n \
                                           To write coordinates of the query residues along\n \
                                           with the found ligands')
        sys.exit(1)

    codes, coords = find_ligands(query, structure, headroom=headroom, write_coords=True, all_coords=True)
    for i in coords:
        print(i)
