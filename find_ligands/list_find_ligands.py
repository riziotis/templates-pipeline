#!/usr/bin/env python3

import sys
import subprocess
from find_ligands import *

het = sys.argv[1]
in_list = sys.argv[2]
space = sys.argv[3]

with open(in_list, 'r', encoding='charmap') as structure_list:
    for structure in structure_list:
        structure = structure.strip()
        outfile = '{}.{}.pdb'.format(structure.strip().split('/')[-1][0:-4], het)
        codes, coords = find_ligands(het, structure, headroom=space, write_coords=True, all_coords=True)
        if coords:
            with open(outfile, 'w') as o:
                for line in coords:
                    print(line, file=o)
